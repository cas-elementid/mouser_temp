# ruby-2.1.4
# gem install libxml-ruby
# (libxml-ruby-2.9.0)
# http://xml4r.github.io/libxml-ruby/rdoc/LibXML/XML/XPath.html
require 'csv'
require 'libxml'
require 'xml'

xml = './xml/Connectors/Wire-to-Board Headers and Receptacles.xml'
csv_file = File.basename(xml).gsub(/\.xml$/,'.csv')
dir = File.dirname(xml).gsub(/^xml/,'csv')

parser = XML::Parser.file(xml)
doc = parser.parse

all_keys = []
# TODO - Switch to fastercsv or something better if needed.
puts "** Open csv"
csv = CSV.open("#{dir}/lib_#{csv_file}", 'wb')

doc.find('/file/class').each do |xml_class|
  total_parts = xml_class.find('part').count
  xml_class.find('part').each_with_index do |part, i|
    puts "Processing part #{i+1} of #{total_parts}"
    part.each_element do |x|
      print '.'
      part_hash = {
        'class_id' => xml_class['id'],
        'class_name' => xml_class.find('text()').to_s.strip,
        'search_keywords' => xml_class.find('search_keywords/text()').to_s.strip,
        'part_id' => part['id']
      }
      if x.count > 0

      else
        part_hash[x.name] = x.content
      end

      csv << part_hash.keys 
      csv << part_hash.values 
      all_keys = all_keys | part_hash.keys
    end
    
  end
end