# Scripts

* download.rb - downloads the zip files into the download directory
* extract.rb  - extracts the zip files into subdirectories inside of xml
* analyze_raw_files.rb - used to get sizings / count of files.
* utils.rb    - common code

Currently am running these two scripts on a large (130MB) file to get a sense of performance bounds.

* xml_to_csv.rb - incomplete.  Will transform xml to csvs.  Attempt to do so in a generic manner.
* xml_to_csv_libxml.rb - same as xml_to_csv.rb but uses libxml.  Appears to be about 3-4x faster.

## Run scripts in order:
download.rb - verify scripts exist in the download directory.
extract.rb  - verify xml available in the xml directory
xml_to_csv.rb - incomplete, just gives an idea of what is coming...
               it currently
               shows that attributes vary not just by file, but by
               individual record.  This suggests each file will end
               up being a relatively sparse matrix.
or
xml_to_csv_libxml.rb - same as xml_to_csv.rb.

# Preliminary Plan

Going to attempt to consider part attributes columns.  Will deviate from this if it becomes evident that there are too many attributes.

* Create code to transform xml to csv.  Preserve as much data as possible.
* Target will be mysql import.
* CSV files will be at the part level.  Attributes will be specified as columns.
* May need to import into multiple tables that support 1-1 relationship with parts.  Such tables would just hold attributes.
* Ideally we would sweep through all files and identify all attributes first.  It looks like this will require an entire pass through of all files two times (!).  If the resulting csv is too wide, it will need to be mapped to multiple tables and loaded several times.

# TODO
* Write xml to csv code
* Time download/extraction process (seems like it will take a few minutes - but less than an 1/2 an hour)
* Integrate with existing File Upload Application

# List of URLs
```
http://www.te.com/usa-en/catalog/datafeed/Antennas.zip
http://www.te.com/usa-en/catalog/datafeed/Application Tooling.zip
http://www.te.com/usa-en/catalog/datafeed/Cable Assemblies.zip
http://www.te.com/usa-en/catalog/datafeed/Circuit Protection.zip
http://www.te.com/usa-en/catalog/datafeed/Connectors.zip
http://www.te.com/usa-en/catalog/datafeed/EMI Filters.zip
http://www.te.com/usa-en/catalog/datafeed/Fiber Optics.zip
http://www.te.com/usa-en/catalog/datafeed/Harnessing.zip
http://www.te.com/usa-en/catalog/datafeed/Heat Shrink Tubing.zip
http://www.te.com/usa-en/catalog/datafeed/Identification and Labeling.zip
http://www.te.com/usa-en/catalog/datafeed/Passive Components.zip
http://www.te.com/usa-en/catalog/datafeed/Power Systems.zip
http://www.te.com/usa-en/catalog/datafeed/Relays Contactors and Switches.zip
http://www.te.com/usa-en/catalog/datafeed/Sensors.zip
http://www.te.com/usa-en/catalog/datafeed/Terminals and Splices.zip
http://www.te.com/usa-en/catalog/datafeed/Wire and Cable.zip
```
