require_relative './utils'

puts "Started Downloading at #{Time.now}"
@list.split("\n").each do |f|
  zip_file = "#{DOWNLOAD_DIR}/#{f}.zip"
  if File.exist?(zip_file)
    puts "#{zip_file} exists."
  else
    download(DOWNLOAD_DIR, f)
  end
end
puts "Ended Downloading at #{Time.now}"
