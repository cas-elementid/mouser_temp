require 'csv'
require 'nokogiri'

def time_execution &block
  start_time =  Time.now
  puts "#{start_time}"

  yield

  end_time = Time.now
  puts "Start: #{start_time}"
  puts "End #{end_time}"
end

def process(xml_file, csv_file, dir)
  puts "** Open XML #{xml_file.path}"
  doc = Nokogiri::XML(File.read(xml_file))
  all_keys = []
  # TODO - Switch to fastercsv or something better if needed.
  puts "** Open csv"
  csv = CSV.open("#{dir}/#{csv_file}", 'wb')

  doc.xpath('/file/class').each do |xml_class|
    total_parts = xml_class.xpath('part').count
    xml_class.xpath('part').each_with_index do |part, i|
      puts "Processing part #{i+1} of #{total_parts}"
      part.elements.each do |x|
        print '.'
        part_hash = {
          'class_id' => xml_class['id'],
          'class_name' => xml_class.xpath('text()').text.strip,
          'search_keywords' => xml_class.xpath('search_keywords/text()').text.strip,
          'part_id' => part['id']
        }
        if x.elements.count > 0
          # TODO
          # make sure we get down to the lowermost
          # element.
          #        puts "*** #{x.name} has #{x.elements.count}"
        else
          part_hash[x.name] = x.content
        end
        # Illustration purposes only... we need to save a list of columns
        # and create a csv that will be a relatively sparse matrix.  Look
        # at the example output and you will see that different attributes
        # are available with each record.
        csv << part_hash.keys
        csv << part_hash.values
        all_keys = all_keys | part_hash.keys
      end

    end
  end

  puts "XML File: #{dir}/#{xml_file} ALL KEYS: count - #{all_keys.count}"
  puts all_keys.sort.join(', ')

end


##
## MAIN PROCESSING
##

time_execution do
  input_xml = File.new('./xml/Wire and Cable/Hook Up Wire.xml')
  target_filename = File.basename(input_xml).gsub(/\.xml$/,'.csv')
  target_dirname = File.dirname(input_xml).gsub(/^xml/,'csv')
  `mkdir -p #{target_dirname.gsub(' ','\\ ')}`
  process(input_xml, target_filename, target_dirname)
end

=begin
require 'find'

xmls = []
Find.find('xml') do |path|
  xmls << path if path =~ /.*\.xml$/
end

start_time =  Time.now
puts "#{start_time}"
xmls.each{|xml|
  csv = File.basename(xml).gsub(/\.xml$/,'.csv')
  dir = File.dirname(xml).gsub(/^xml/,'csv')
  `mkdir -p #{dir.gsub(' ','\\ ')}`
  process(xml, csv, dir)
}
end_time = Time.now
puts "#{end_time}"
=end
