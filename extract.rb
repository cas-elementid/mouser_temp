require_relative './utils'

puts 'XML Directory'
puts `du -sh #{DOWNLOAD_DIR}`
puts `mkdir -p #{XML_DIR}`

puts "Started Extracting at #{Time.now}"
Dir.new(DOWNLOAD_DIR).entries.each do|f|
  unless ['.', '..'].include? f
    zip_file = f.gsub(' ', '\\ ')
    subdir = zip_file.gsub('.zip', '') # Duplicate file names require subdirs
    exec ("mkdir -p #{XML_DIR}/#{subdir} && "\
      "cd #{XML_DIR}/#{subdir} && "\
      "unzip ../../#{DOWNLOAD_DIR}/#{zip_file}")
  end
end

puts "Ended Extracting at #{Time.now}"
puts 'Download Directory'
puts `du -sh #{XML_DIR}`
