require 'open-uri'
require 'uri'

DOWNLOAD_DIR = 'download'
XML_DIR = 'xml'

@list = <<EOF
Antennas
Application Tooling
Cable Assemblies
Circuit Protection
Connectors
EMI Filters
Fiber Optics
Harnessing
Heat Shrink Tubing
Identification and Labeling
Passive Components
Power Systems
Relays Contactors and Switches
Sensors
Terminals and Splices
Wire and Cable
EOF

def te_url(name)
  "http://www.te.com/usa-en/catalog/datafeed/#{URI.escape(name)}.zip"
end

def download(dir, name)
  `mkdir -p #{dir}`
  url = te_url(name)
  puts url
  File.write("#{dir}/#{name}.zip", open(url).read)
end

def exec(cmd)
  puts cmd
  puts `#{cmd}`
end
