

puts "Total Files:       #{`ls xml/*/*.xml | wc -l`}"
puts "Unique File Names: #{`ls xml/*/*.xml | awk -F'/' '{print $3}' | sort | uniq | wc -l`}"
puts "File Listing:      #{`/opt/local/bin/tree xml`}"

a = `ls -l xml/*/*.xml`.split("\n")
results = []
a.each do |l|
  f = l.split('/').last
  s = l.split[4]
  puts "#{f} #{s}"
  results << [s, f]
end; nil

results.sort_by { |i| i.first.to_i }.each do|x|
  puts "#{x[0].ljust(10)} #{x[1]}"
end; nil
